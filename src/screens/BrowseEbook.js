import React, { Component } from 'react'
import {
	Dimensions,
	Image,
	StyleSheet,
	ScrollView,
	TouchableOpacity,
	ActivityIndicator,
} from 'react-native'
import * as firebase from 'firebase'

import { Card, Badge, Button, Block, Text } from '../components'
import { theme, mocks } from '../constants'

const { width } = Dimensions.get('window')

class BrowseEbook extends Component {
	constructor() {
		super()

		this.state = {
			active: 'Ebooks',
			email: '',
			username: '',
			url: '',
			loading: true,
		}
	}

	componentDidMount() {
		this.getUser()
	}

	async getUser() {
		let authEmail = firebase.auth().currentUser.email
		firebase
			.database()
			.ref('users')
			.once('value')
			.then(snapshot => {
				snapshot.forEach(child => {
					if (authEmail === child.val().email) {
						const email = child.val().email
						const username = child.val().username
						this.setState({ email: email, username: username })
						this.getAndLoadHttpUrl()
					}
				})
			})
	}

	async getAndLoadHttpUrl() {
		let uid = firebase.auth().currentUser.uid
		const ref = firebase.storage().ref('profile-images/' + uid)
		ref.getDownloadURL()
			.then(data => {
				this.setState({ url: data })
				this.setState({ loading: false })
			})
			.catch(error => {
				console.log(error.message + 'nacalaa')
			})
	}

	renderTab(tab) {
		const { active } = this.state
		const isActive = active === tab

		return (
			<TouchableOpacity
				key={`tab-${tab}`}
				onPress={() => this.setState({ active: tab })}
				style={[styles.tab, isActive ? styles.active : null]}
			>
				<Text title medium gray={isActive} secondary={isActive}>
					{tab}
				</Text>
			</TouchableOpacity>
		)
	}

	render() {
		const { profile, ebooks, audiobooks, navigation } = this.props
		const tabs = ['Ebooks', 'AudioBooks']

		if (this.state.loading) {
			return (
				<Block center middle>
					<ActivityIndicator
						size="large"
						color={theme.colors.primary}
					/>
				</Block>
			)
		}
		return (
			<Block>
				<Block
					flex={false}
					row
					center
					space="between"
					style={styles.header}
				>
					<Text h1 bold>
						Browse
					</Text>
					<Button onPress={() => navigation.navigate('Profile')}>
						<Image
							source={{ uri: this.state.url }}
							style={styles.avatar}
						/>
					</Button>
				</Block>

				<Block flex={false} row style={styles.tabs}>
					{tabs.map(tab => this.renderTab(tab))}
				</Block>

				<ScrollView
					ShowVerticalScrollIndicator={false}
					style={{ paddingVertical: theme.sizes.base * 2 }}
				>
					<Block
						flex={false}
						row
						space="between"
						style={styles.ebooksContainer}
					>
						{this.state.active === 'Ebooks'
							? ebooks.map(ebook => (
									<TouchableOpacity key={ebook.id}>
										<Card
											center
											middle
											shadow
											style={styles.ebooks}
										>
											<Badge
												margin={[0, 0, 15]}
												size={50}
												color="rgba(41,216,143,0.20)"
											>
												<Image
													source={ebook.image}
													style={styles.ebookImages}
												/>
											</Badge>

											<Text
												center
												semibold
												medium
												height={20}
											>
												{ebook.name}
											</Text>
											<Text gray caption>
												{ebook.read} Read
											</Text>
										</Card>
									</TouchableOpacity>
							  ))
							: audiobooks.map(audiobook => (
									<TouchableOpacity key={audiobook.id}>
										<Card
											center
											middle
											shadow
											style={styles.ebooks}
										>
											<Badge
												margin={[0, 0, 15]}
												size={50}
												color="rgba(41,216,143,0.20)"
											>
												<Image
													source={audiobook.image}
													style={styles.ebookImages}
												/>
											</Badge>

											<Text
												center
												semibold
												medium
												height={20}
											>
												{audiobook.name}
											</Text>
											<Text gray caption>
												{audiobook.listen} Listen
											</Text>
										</Card>
									</TouchableOpacity>
							  ))}
					</Block>
				</ScrollView>
			</Block>
		)
	}
}

BrowseEbook.defaultProps = {
	profile: mocks.Profile,
	ebooks: mocks.Ebooks,
	audiobooks: mocks.AudioBooks,
}

const styles = StyleSheet.create({
	header: {
		paddingHorizontal: theme.sizes.base * 2,
	},

	avatar: {
		height: theme.sizes.base * 2.2,
		width: theme.sizes.base * 2.2,
		borderRadius: (theme.sizes.base * 2.2) / 2,
	},

	tabs: {
		borderBottomColor: theme.colors.gray,
		borderBottomWidth: StyleSheet.hairlinewidth,
		marginVertical: theme.sizes.base,
		marginHorizontal: theme.sizes.base * 2,
	},

	tab: {
		marginRight: theme.sizes.base * 2,
		paddingBottom: theme.sizes.base,
	},

	active: {
		borderBottomColor: theme.colors.secondary,
		borderBottomWidth: 3,
	},

	ebooksContainer: {
		flexWrap: 'wrap',
		paddingHorizontal: theme.sizes.base * 2,
		marginBottom: theme.sizes.base * 3.5,
	},

	ebooks: {
		width: 150,
		height: 150,
	},

	ebookImages: {
		width: 60,
		height: 100 + '%',
	},
})

export default BrowseEbook
