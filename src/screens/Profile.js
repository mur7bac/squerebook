import React, { Component } from 'react'
import {
    Image,
    StyleSheet,
    ScrollView,
    TextInput,
    ActivityIndicator,
} from 'react-native'
import { Icon } from 'react-native-elements'
import firebase from 'firebase'
import * as ImagePicker from 'expo-image-picker'
import * as Permissions from 'expo-permissions'

import { Divider, Button, Block, Text, Switch } from '../components'
import { theme, mocks } from '../constants'

class Profile extends Component {
    state = {
        email: '',
        username: '',
        url: '',
        loading: true,
        editing: null,
        profile: {},
    }

    async getUser() {
        let authEmail = firebase.auth().currentUser.email
        firebase
            .database()
            .ref('users')
            .once('value')
            .then(snapshot => {
                snapshot.forEach(child => {
                    if (authEmail === child.val().email) {
                        const email = child.val().email
                        const username = child.val().username
                        const fullName = child.val().name
                        this.setState({
                            email: email,
                            username: username,
                            name: fullName,
                            profile: child.val(),
                        })
                        console.log(child.val())
                        this.getAndLoadHttpUrl()
                    }
                })
            })
    }

    async updateUser(name) {
        const { profile } = this.state
        let user = firebase.auth().currentUser.uid
        let cuser = firebase.auth().currentUser
        firebase
            .database()
            .ref('/users/' + user)
            .once('value')
            .then(function(snapshot) {
                // var username =
                //     (snapshot.val() && snapshot.val().username) || 'Anonymous'
                // ...
                console.log(snapshot.val())
            })
        console.log(name)

        if (name === 'name') {
            firebase
                .database()
                .ref('users/' + user + '/name')
                .set(profile.name)
        }

        if (name === 'password') {
            firebase
                .database()
                .ref('users/' + user + '/password')
                .set(profile.password)

            cuser
                .updatePassword(profile.password)
                .then(() => {
                    console.log('Update successful.')
                })
                .catch(error => {
                    console.log('An error happened.' + error)
                })
        }

        if (name === 'username') {
            firebase
                .database()
                .ref('users/' + user + '/username')
                .set(profile.username)
        }
    }

    async getAndLoadHttpUrl() {
        let uid = firebase.auth().currentUser.uid
        const ref = firebase.storage().ref('profile-images/' + uid)
        ref.getDownloadURL()
            .then(data => {
                this.setState({ url: data })
                this.setState({ loading: false })
            })
            .catch(error => {
                console.log(error.message + ' nacalaa')
            })
    }

    componentDidMount() {
        this.getUser()
    }

    onUploadImage = async () => {
        let userId = firebase.auth().currentUser.uid
        const { status_roll } = await Permissions.askAsync(
            Permissions.CAMERA_ROLL
        )
        if (status_roll !== 'granted') {
            alert(status_roll)
        }
        let pickerResult = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [2, 3],
        })

        if (!pickerResult.cancelled) {
            this.uploadImage(pickerResult.uri, userId)
                .then(() => {
                    console.log('Image is choosen')
                    console.log(userId)
                })
                .catch(error => {
                    console.log(error)
                })
        }
    }

    uploadImage = async (uri, imageName) => {
        const response = await fetch(uri)
        const blob = await response.blob()

        let ref = firebase
            .storage()
            .ref()
            .child('profile-images/' + imageName)

        ref.put(blob)
            .then(() => {
                console.log('Success')
            })
            .catch(error => {
                console.log(error)
            })
    }

    handleEdit(name, text) {
        const { profile } = this.state
        profile[name] = text

        this.setState({ profile })
    }

    toggleEdit(name) {
        const { editing } = this.state
        this.setState({ editing: !editing ? name : null })
    }

    renderEdit(name) {
        const { profile, editing } = this.state

        if (editing === name) {
            this.updateUser(editing)
            return (
                <TextInput
                    defaultValue={profile[name]}
                    onChangeText={text => this.handleEdit([name], text)}
                />
            )
        }

        return <Text bold>{profile[name]}</Text>
    }

    render() {
        const { profile, editing } = this.state

        if (this.state.loading) {
            return (
                <Block center middle>
                    <ActivityIndicator
                        size="large"
                        color={theme.colors.primary}
                    />
                </Block>
            )
        }

        return (
            <Block>
                <Block
                    flex={false}
                    row
                    center
                    space="between"
                    style={styles.header}
                >
                    <Text h1 bold>
                        Profile
                    </Text>
                    <Button>
                        <Image
                            source={{ uri: this.state.url }}
                            style={styles.avatar}
                        />
                    </Button>
                </Block>

                <ScrollView showsVerticalScrollIndicator={false}>
                    <Block style={styles.inputs}>
                        <Block
                            row
                            space="between"
                            margin={[10, 0]}
                            style={styles.inputRow}
                        >
                            <Block
                                row
                                space="between"
                                margin={[10, 0]}
                                style={styles.inputRow}
                            >
                                <Block>
                                    <Text gray2 style={{ marginBottom: 10 }}>
                                        E-mail
                                    </Text>
                                    <Text bold>{this.state.email}</Text>
                                </Block>
                            </Block>
                        </Block>
                        <Block
                            row
                            space="between"
                            margin={[10, 0]}
                            style={styles.inputRow}
                        >
                            <Block>
                                <Text gray2 style={{ marginBottom: 10 }}>
                                    Full Name
                                </Text>
                                {this.renderEdit('name')}
                            </Block>
                            <Text
                                medium
                                secondary
                                onPress={() => this.toggleEdit('name')}
                            >
                                {editing === 'name' ? 'Save' : 'Edit'}
                            </Text>
                        </Block>
                        <Block
                            row
                            space="between"
                            margin={[10, 0]}
                            style={styles.inputRow}
                        >
                            <Block>
                                <Text gray2 style={{ marginBottom: 10 }}>
                                    Username
                                </Text>
                                {this.renderEdit('username')}
                            </Block>
                            <Text
                                medium
                                secondary
                                onPress={() => this.toggleEdit('username')}
                            >
                                {editing === 'username' ? 'Save' : 'Edit'}
                            </Text>
                        </Block>
                        <Block
                            row
                            space="between"
                            margin={[10, 0]}
                            style={styles.inputRow}
                        >
                            <Block>
                                <Text gray2 style={{ marginBottom: 10 }}>
                                    Password
                                </Text>
                                {this.renderEdit('password')}
                            </Block>
                            <Text
                                secure
                                medium
                                secondary
                                onPress={() => this.toggleEdit('password')}
                            >
                                {editing === 'password' ? 'Save' : 'Edit'}
                            </Text>
                        </Block>
                        <Button
                            shadow
                            style={[styles.image]}
                            onPress={() => this.onUploadImage()}
                        >
                            <Block
                                center
                                middle
                                style={{ paddingHorizontal: 20 }}
                                flex={false}
                                row
                                space="between"
                            >
                                <Text gray caption>
                                    Update Profile Image
                                </Text>
                                <Icon
                                    name="upload"
                                    type="font-awesome"
                                    color="#517fa4"
                                />
                            </Block>
                        </Button>
                    </Block>
                </ScrollView>
            </Block>
        )
    }
}

export default Profile

const styles = StyleSheet.create({
    header: {
        paddingHorizontal: theme.sizes.base * 2,
    },
    avatar: {
        height: theme.sizes.base * 2.2,
        width: theme.sizes.base * 2.2,
        borderRadius: (theme.sizes.base * 2.2) / 2,
    },

    inputs: {
        marginTop: theme.sizes.base * 0.7,
        paddingHorizontal: theme.sizes.base * 2,
    },
    inputRow: {
        alignItems: 'flex-end',
    },
    sliders: {
        marginTop: theme.sizes.base * 0.7,
        paddingHorizontal: theme.sizes.base * 2,
    },
    thumb: {
        width: theme.sizes.base,
        height: theme.sizes.base,
        borderRadius: theme.sizes.base,
        borderColor: 'white',
        borderWidth: 3,
        backgroundColor: theme.colors.secondary,
    },
    toggles: {
        paddingHorizontal: theme.sizes.base * 2,
    },
    image: {
        borderRadius: 0,
        borderWidth: 0,
        borderColor: theme.colors.gray2,
        borderWidth: StyleSheet.hairlineWidth,
    },
})
