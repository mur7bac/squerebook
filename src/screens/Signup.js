import React, { Component } from 'react'
import {
    Alert,
    ActivityIndicator,
    Keyboard,
    KeyboardAvoidingView,
    StyleSheet,
} from 'react-native'
import { Icon } from 'react-native-elements'

import { Button, Block, Input, Text } from '../components'
import { theme } from '../constants'
import * as firebase from 'firebase'
import * as ImagePicker from 'expo-image-picker'
import * as Permissions from 'expo-permissions'

let app = firebase.initializeApp({
    apiKey: 'AIzaSyCvp--I0IEANlEq6_qW-pJLkFRmMuD7zBI',
    authDomain: 'squarebook-ca331.firebaseapp.com',
    databaseURL: 'https://squarebook-ca331.firebaseio.com',
    projectId: 'squarebook-ca331',
    storageBucket: 'squarebook-ca331.appspot.com',
    messagingSenderId: '653658271888',
    appId: '1:653658271888:web:612c17aca9a831b56ee815',
    measurementId: 'G-BNM20LGBN7',
})

let db = app.database()

export default class SignUp extends Component {
    constructor() {
        super()

        this.state = {
            email: null,
            username: null,
            name: null,
            password: null,
            image: false,
            errors: [],
            loading: false,
            usernames: [],
            uri: null,
        }

        // Get usernames from database to check if the entered username exists or not

        let users = []
        db.ref('/users/')
            .once('value')
            .then(snapshot => {
                snapshot.forEach(childSnapshot => {
                    users.push(childSnapshot.val().username)
                })

                this.setState({ usernames: users })
            })
    }

    getUsers = username => {
        const errors = []
        db.ref('/users/')
            .once('value')
            .then(snapshot => {
                snapshot.forEach(childSnapshot => {
                    if (username == childSnapshot.val().username) {
                        return false
                        alert('Username Exists')
                    } else {
                        this.setState({ usernames: true })
                        return true
                    }
                })
            })
    }

    addUser = ({ username, password, email, name }) => {
        let uid = firebase.auth().currentUser.uid
        db.ref()
            .child('/users')
            .child(uid)
            .set({
                name: name,
                username: username,
                password: password,
                email: email,
            })
    }

    onUploadImage = async () => {
        const { status_roll } = await Permissions.askAsync(
            Permissions.CAMERA_ROLL
        )
        if (status_roll !== 'granted') {
            console.log('Hey! You heve not enabled selected permissions')
        }
        let pickerResult = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [2, 3],
        })

        if (!pickerResult.cancelled) {
            this.setState({ uri: pickerResult.uri, image: true })
        }
    }

    uploadImage = async ({ uri }) => {
        let uid = firebase.auth().currentUser.uid
        const response = await fetch(uri)
        const blob = await response.blob()

        let ref = firebase
            .storage()
            .ref()
            .child('profile-images/' + uid)

        ref.put(blob)
    }

    handleSignUp() {
        const { navigation } = this.props
        const { email, password, username, image, name, usernames } = this.state
        const errors = []

        Keyboard.dismiss()
        this.setState({ loading: true })

        // check with backend API or with some static data
        if (!email) {
            this.setState({ errors, loading: false })
            errors.push('email')
        }
        if (!password) {
            this.setState({ errors, loading: false })
            errors.push('password')
        }

        if (!username) {
            this.setState({ errors, loading: false })
            errors.push('username')
        }

        if (!name) {
            this.setState({ errors, loading: false })
            errors.push('name')
        }

        if (!image) {
            this.setState({ errors, loading: false })
            errors.push('image')
        }

        if (usernames.includes(username)) {
            this.setState({ errors, loading: false })
            alert('Username Already Exists')
            errors.push('username')
        }

        if (!errors.length) {
            firebase
                .auth()
                .createUserWithEmailAndPassword(email, password)
                .then(() => {
                    this.setState({ errors, loading: false })
                    this.uploadImage(this.state)
                    this.addUser(this.state)
                    navigation.navigate('BrowseEbook')
                })
                .catch(error => {
                    this.setState({
                        errors: 'Authentication Failed',
                        loading: false,
                    })
                    alert(error)
                })
        }
    }

    render() {
        const { navigation } = this.props
        const { loading, errors } = this.state
        const hasErrors = key =>
            errors.includes(key) ? styles.hasErrors : null

        return (
            <KeyboardAvoidingView
                style={styles.signup}
                behavior="padding"
                keyboardVerticalOffset={-100}
            >
                <Block padding={[0, theme.sizes.base * 2]}>
                    <Text h1 bold>
                        Sign Up
                    </Text>
                    <Block middle>
                        <Input
                            label="Full Name"
                            error={hasErrors('name')}
                            style={[styles.input, hasErrors('name')]}
                            defaultValue={this.state.name}
                            onChangeText={text => this.setState({ name: text })}
                        />
                        <Input
                            label="Username"
                            error={hasErrors('username')}
                            style={[styles.input, hasErrors('username')]}
                            defaultValue={this.state.username}
                            onChangeText={text =>
                                this.setState({ username: text })
                            }
                        />
                        <Input
                            email
                            label="Email"
                            error={hasErrors('email')}
                            style={[styles.input, hasErrors('email')]}
                            defaultValue={this.state.email}
                            onChangeText={text =>
                                this.setState({ email: text })
                            }
                        />
                        <Input
                            secure
                            label="Password"
                            error={hasErrors('password')}
                            style={[styles.input, hasErrors('password')]}
                            defaultValue={this.state.password}
                            onChangeText={text =>
                                this.setState({ password: text })
                            }
                        />
                        <Button
                            shadow
                            error={hasErrors('image')}
                            style={[styles.image, hasErrors('image')]}
                            onPress={() => this.onUploadImage()}
                        >
                            <Block
                                center
                                middle
                                style={{ paddingHorizontal: 20 }}
                                flex={false}
                                row
                                space="between"
                            >
                                <Text gray caption>
                                    Upload Profile Image
                                </Text>
                                <Icon
                                    name="upload"
                                    type="font-awesome"
                                    color="#517fa4"
                                />
                            </Block>
                        </Button>
                        <Button gradient onPress={() => this.handleSignUp()}>
                            {loading ? (
                                <ActivityIndicator size="small" color="white" />
                            ) : (
                                <Text bold white center>
                                    Sign Up
                                </Text>
                            )}
                        </Button>

                        <Button onPress={() => navigation.navigate('Login')}>
                            <Text
                                gray
                                caption
                                center
                                style={{ textDecorationLine: 'underline' }}
                            >
                                Back to Login
                            </Text>
                        </Button>
                    </Block>
                </Block>
            </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    signup: {
        flex: 1,
        justifyContent: 'center',
    },
    input: {
        borderRadius: 0,
        borderWidth: 0,
        borderBottomColor: theme.colors.gray2,
        borderBottomWidth: StyleSheet.hairlineWidth,
        height: 30,
    },
    hasErrors: {
        borderBottomColor: theme.colors.accent,
        borderColor: theme.colors.accent,
    },
    image: {
        borderRadius: 0,
        borderWidth: 0,
        borderColor: theme.colors.gray2,
        borderWidth: StyleSheet.hairlineWidth,
    },
})
