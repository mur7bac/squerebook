import React, { Component } from 'react'
import {
    ActivityIndicator,
    Keyboard,
    StyleSheet,
    KeyboardAvoidingView,
} from 'react-native'

import { Block, Text, Button, Input } from '../components/'
import { theme } from '../constants/'
import * as firebase from 'firebase'

class Login extends Component {
    state = {
        email: '',
        password: '',
        errors: [],
        loading: false,
    }

    handleLogin() {
        const { navigation } = this.props
        const { email, password } = this.state
        const errors = []

        Keyboard.dismiss()
        this.setState({ loading: true })
        // check with backend API or with some static data
        if (!email) {
            this.setState({ errors, loading: false })
            errors.push('email')
        }
        if (!password) {
            this.setState({ errors, loading: false })
            errors.push('password')
        }

        if (!errors.length) {
            firebase
                .auth()
                .signInWithEmailAndPassword(email, password)
                .then(() => {
                    this.setState({ errors, loading: false })
                    navigation.navigate('BrowseEbook')
                })
                .catch(() => {
                    this.setState({
                        errors: 'Authentication Failed',
                        loading: false,
                    })
                    alert('Please Check your Email and Password')
                })
        }
    }

    render() {
        const { navigation } = this.props
        const { loading, errors } = this.state
        const hasErrors = key =>
            errors.includes(key) ? styles.hasErrors : null

        return (
            <KeyboardAvoidingView style={styles.login} behavior="padding">
                <Block padding={[0, theme.sizes.base * 2]}>
                    <Text h1 bold>
                        Login
                    </Text>
                    <Block middle>
                        <Input
                            autoFocus={true}
                            label="Email"
                            error={hasErrors('email')}
                            style={styles.input}
                            defaultValue={this.state.email}
                            onChangeText={text =>
                                this.setState({ email: text })
                            }
                        />
                        <Input
                            secure
                            label="Password"
                            error={hasErrors('password')}
                            style={styles.input}
                            defaultValue={this.state.password}
                            onChangeText={text =>
                                this.setState({ password: text })
                            }
                        />
                        <Button gradient onPress={() => this.handleLogin()}>
                            {loading ? (
                                <ActivityIndicator size="small" color="white" />
                            ) : (
                                <Text bold white center>
                                    Login
                                </Text>
                            )}
                        </Button>
                        <Button
                            onPress={() => {
                                navigation.navigate('Forgot')
                            }}
                        >
                            <Text
                                gray2
                                caption
                                center
                                style={{ textDecorationLine: 'underline' }}
                            >
                                Forgot your Password
                            </Text>
                        </Button>
                    </Block>
                </Block>
            </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    login: {
        flex: 1,
        justifyContent: 'center',
    },

    input: {
        borderRadius: 0,
        borderWidth: 0,
        borderBottomColor: theme.colors.gray2,
        borderBottomWidth: StyleSheet.hairlineWidth,
    },

    hasErrors: {
        borderBottomColor: theme.colors.accent,
    },
})

export default Login
