const Categories = []

const Ebooks = [
    {
        id: 1,
        name: 'the four hour work week',
        tags: ['Ebook, '],
        description:
            '#1 New York Times best-selling author Tim Ferriss, teaches you how to escape the 9-5, live anywhere, and join the new rich.',
        image: require('../assets/Ebook/1.jpg'),
        read: '30min',
    },
    {
        id: 2,
        name: 'the four hour work week',
        tags: ['Ebook, '],
        description:
            '#1 New York Times best-selling author Tim Ferriss, teaches you how to escape the 9-5, live anywhere, and join the new rich.',
        image: require('../assets/Ebook/1.jpg'),
        read: '30min',
    },
    {
        id: 3,
        name: 'the four hour work week',
        tags: ['Ebook, '],
        description:
            '#1 New York Times best-selling author Tim Ferriss, teaches you how to escape the 9-5, live anywhere, and join the new rich.',
        image: require('../assets/Ebook/1.jpg'),
        read: '30min',
    },
    {
        id: 4,
        name: 'the four hour work week',
        tags: ['Ebook, '],
        description:
            '#1 New York Times best-selling author Tim Ferriss, teaches you how to escape the 9-5, live anywhere, and join the new rich.',
        image: require('../assets/Ebook/1.jpg'),
        read: '30min',
    },
    {
        id: 5,
        name: 'the four hour work week',
        tags: ['Ebook, '],
        description:
            '#1 New York Times best-selling author Tim Ferriss, teaches you how to escape the 9-5, live anywhere, and join the new rich.',
        image: require('../assets/Ebook/1.jpg'),
        read: '30min',
    },
    {
        id: 6,
        name: 'the four hour work week',
        tags: ['Ebook, '],
        description:
            '#1 New York Times best-selling author Tim Ferriss, teaches you how to escape the 9-5, live anywhere, and join the new rich.',
        image: require('../assets/Ebook/1.jpg'),
        read: '30min',
    },
]

const AudioBooks = [
    {
        id: 1,
        name: 'the four hour work week',
        tags: ['Ebook, '],
        description:
            '#1 New York Times best-selling author Tim Ferriss, teaches you how to escape the 9-5, live anywhere, and join the new rich.',
        image: require('../assets/AudioBook/2.jpg'),
        listen: '30min',
    },
    {
        id: 2,
        name: 'the four hour work week',
        tags: ['AudioBook, '],
        description:
            '#1 New York Times best-selling author Tim Ferriss, teaches you how to escape the 9-5, live anywhere, and join the new rich.',
        image: require('../assets/AudioBook/2.jpg'),
        listen: '30min',
    },
    {
        id: 3,
        name: 'The four hour work week',
        tags: ['AudioBook, '],
        description:
            '#1 New York Times best-selling author Tim Ferriss, teaches you how to escape the 9-5, live anywhere, and join the new rich.',
        image: require('../assets/AudioBook/2.jpg'),
        listen: '30min',
    },
    {
        id: 4,
        name: 'the four hour work week',
        tags: ['AudioBook, '],
        description:
            '#1 New York Times best-selling author Tim Ferriss, teaches you how to escape the 9-5, live anywhere, and join the new rich.',
        image: require('../assets/AudioBook/2.jpg'),
        listen: '30min',
    },
    {
        id: 5,
        name: 'the four hour work week',
        tags: ['AudioBook, '],
        description:
            '#1 New York Times best-selling author Tim Ferriss, teaches you how to escape the 9-5, live anywhere, and join the new rich.',
        image: require('../assets/AudioBook/2.jpg'),
        listen: '30min',
    },
    {
        id: 6,
        name: 'the four hour work week',
        tags: ['AudioBook, '],
        description:
            '#1 New York Times best-selling author Tim Ferriss, teaches you how to escape the 9-5, live anywhere, and join the new rich.',
        image: require('../assets/AudioBook/2.jpg'),
        listen: '30min',
    },
]

const Profile = {
    username: 'mur7bac',
    location: 'Hero awr',
    email: 'mur7bac@gmail.com',
    avatar: require('../assets/Profile/murabac.jpg'),
    Ebooks: 10,
    AudioBooks: 20,
}

export { Categories, Ebooks, AudioBooks, Profile }
