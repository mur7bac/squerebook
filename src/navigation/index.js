import React from 'react'
import { Image } from 'react-native'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'

import Welcome from '../screens/Welcome'
import Login from '../screens/Login'
import Signup from '../screens/Signup'
import Forgot from '../screens/Forgot'
import BrowseEbook from '../screens/BrowseEbook'
// import BrowseAudiobook from '../screens/BrowseAudiobook'
// import Ebook from '../screens/Ebook'
// import Audiobook from '../screens/Audiobook'
import Profile from '../screens/Profile'

import { theme } from '../constants'

const screen = createStackNavigator(
    {
        Welcome,
        Login,
        Signup,
        Forgot,
        BrowseEbook,
        // BrowseAudiobook,
        // Ebook,
        // Audiobook,
        Profile,
    },
    {
        defaultNavigationOptions: {
            headerStyle: {
                height: theme.sizes.base * 4,
                backgroundColor: '#fff',
                borderBottomColor: 'transparent',
                evelation: 0,
            },
            headerBackImage: (
                <Image source={require('../assets/icons/back.png')} />
            ),
            headerBackTitle: null,
            headerLeftContainerStyle: {
                alignItems: 'center',
                marginLeft: theme.sizes.base * 2,
                paddingRight: theme.sizes.base * 2,
            },
            headerRightContainerStyle: {
                alignItems: 'center',
                paddingRight: theme.sizes.base * 2,
            },
        },
    }
)

export default createAppContainer(screen)
