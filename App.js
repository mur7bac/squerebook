import React from 'react'
import { StyleSheet, Image, View } from 'react-native'
import { AppLoading } from 'expo'
import { Asset } from 'expo-asset'

import Navigation from './src/navigation'
import * as constants from './src/constants/'
import { Block } from './src/components/'

// import all used images
function importAll(r) {
    return r.keys().map(r)
}

const images = [
    require('./src/assets/Ebook/1.jpg'),
    require('./src/assets/AudioBook/2.jpg'),
    require('./src/assets/Profile/murabac.jpg'),
]

export default class App extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            isLoadingComplete: false,
        }
    }

    handleResourcesAsync = async () => {
        // Simple function that catches all the images for better performance
        const cacheImages = images.map(img => {
            return Asset.fromModule(img).downloadAsync()
        })

        return Promise.all(cacheImages)
    }
    render() {
        if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
            return (
                <AppLoading
                    startAsync={this.handleResourcesAsync}
                    onError={error => console.log(error)}
                    onFinish={() =>
                        this.setState({
                            isLoadingComplete: true,
                        })
                    }
                />
            )
        }

        return (
            <Block>
                <Navigation />
            </Block>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
})
